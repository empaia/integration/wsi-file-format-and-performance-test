import importlib
import os
import pkgutil

import wsi_file_format_and_performance_test.reader_template as reader_template


def get_data_path():
    return os.path.join(os.path.dirname(__file__), "data")


def get_reader_classes():
    path_to_readers = os.path.join(os.path.dirname(__file__), "reader")
    reader_module_import_base = "wsi_file_format_and_performance_test.reader."
    if (
        len(reader_template.SlideReader.__subclasses__())
        + len(reader_template.SlideReaderAsync.__subclasses__())
        == 0
    ):
        for _, name, _ in pkgutil.iter_modules([path_to_readers]):
            importlib.import_module(reader_module_import_base + name)
    return (
        reader_template.SlideReader.__subclasses__()
        + reader_template.SlideReaderAsync.__subclasses__()
    )
