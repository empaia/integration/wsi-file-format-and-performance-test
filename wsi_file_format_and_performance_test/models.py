import hashlib
import json
import os
from typing import List, Literal, Optional
from zipfile import ZipFile

import requests
from pydantic import BaseModel as PydanticBaseModel

allowed_formats = Literal["svs", "tiff", "mrxs"]
allowed_licenses = Literal["cc0"]


class BaseModel(PydanticBaseModel):
    class Config:
        extra = "forbid"


class FileInfo(BaseModel):
    filehash_sha256: str
    filesize: int
    tiff_based: bool
    file_name: str
    internal_file_list: Optional[List[str]]


class ImageSizeWithDepth(BaseModel):
    width: int = None
    height: int = None
    depth: int = None


class ImageSize(BaseModel):
    width: int = None
    height: int = None


class PixelSizeInMicrons(BaseModel):
    x: float = None
    y: float = None
    z: float = None


class SlideTestResult(BaseModel):
    image_size: bool
    tile_size: Optional[bool]
    pixel_size: Optional[bool]
    channels: bool
    bits: bool
    levels: bool
    macro_size: Optional[bool]
    label_size: Optional[bool]
    thumbnail_size: Optional[bool]
    image_tile_mean: Optional[List[bool]]
    image_region_mean: Optional[List[bool]]
    image_macro_mean: Optional[bool]
    image_label_mean: Optional[bool]
    image_thumbnail_mean: Optional[bool]
    tile_performance: Optional[int]
    region_performance: Optional[int]
    tile_performance_io: Optional[int]
    region_performance_io: Optional[int]

    def save(self, output_path):
        with open(output_path, "w") as f:
            f.write(json.dumps(self.dict(), indent=2))


class SlideInfo(BaseModel):
    reader: str
    image_size: ImageSizeWithDepth
    tile_size: Optional[ImageSize]
    pixel_size: Optional[PixelSizeInMicrons]
    channels: int
    bits: int
    levels: List[ImageSize]
    macro_size: Optional[ImageSize]
    label_size: Optional[ImageSize]
    thumbnail_size: Optional[ImageSize]

    def save(self, output_path):
        with open(output_path, "w") as f:
            f.write(json.dumps(self.dict(), indent=2))

    def compare(self, other):
        return SlideTestResult(
            image_size=(self.image_size == other.image_size),
            tile_size=(self.tile_size == other.tile_size if self.tile_size else None),
            pixel_size=(
                self.pixel_size == other.pixel_size if self.pixel_size else None
            ),
            channels=(self.channels == other.channels),
            bits=(self.bits == other.bits),
            levels=(self.levels == other.levels),
            macro_size=(
                self.macro_size == other.macro_size if self.macro_size else None
            ),
            label_size=(
                self.label_size == other.label_size if self.label_size else None
            ),
            thumbnail_size=(
                self.thumbnail_size == other.thumbnail_size
                if self.thumbnail_size
                else None
            ),
        )


class ImageTileMean(BaseModel):
    level: int
    tile_x: int
    tile_y: int
    mean: float


class ImageRegionMean(BaseModel):
    level: int
    start_x: int
    start_y: int
    size_x: int
    size_y: int
    mean: float


class SlideData(BaseModel):
    image_tile_mean: List[ImageTileMean]
    image_region_mean: List[ImageRegionMean]
    image_macro_mean: Optional[float]
    image_label_mean: Optional[float]
    image_thumbnail_mean: Optional[float]


class SlideFile(BaseModel):
    name: str
    format: allowed_formats
    source: str
    license: allowed_licenses
    download_urls: List[str]
    fileinfo: FileInfo
    slideinfo: SlideInfo
    slidedata: Optional[SlideData]

    def download(self, download_folder):
        file_path = os.path.join(download_folder, self.fileinfo.file_name)
        if not os.path.exists(file_path):
            print("download", file_path)
            for download_url in self.download_urls:
                r = requests.get(download_url, allow_redirects=True)
                if r.status_code == 200:
                    break
            with open(file_path, "wb") as f:
                f.write(r.content)
        self._check_file_correct(file_path)
        if file_path.endswith("zip") and self.fileinfo.internal_file_list:
            slide_filepath = os.path.join(
                download_folder, self.fileinfo.internal_file_list[0]
            )
            if not os.path.exists(slide_filepath):
                ZipFile(file_path).extractall(download_folder)
            self._check_internal_file_list(
                download_folder, self.fileinfo.internal_file_list
            )
        else:
            slide_filepath = file_path
        return slide_filepath

    def _check_file_correct(self, file_path):
        assert os.path.getsize(file_path) == self.fileinfo.filesize
        assert self._hash_sha256(file_path) == self.fileinfo.filehash_sha256

    def _hash_sha256(self, filepath):
        hashfile = hashlib.sha256()
        with open(filepath, "rb") as f:
            hashfile.update(f.read())
        return hashfile.hexdigest()

    def _check_internal_file_list(self, download_folder, file_list):
        for file in file_list:
            assert os.path.exists(os.path.join(download_folder, file))
