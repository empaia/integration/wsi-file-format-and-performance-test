from wsi_file_format_and_performance_test.models import (
    ImageSize,
    ImageSizeWithDepth,
    PixelSizeInMicrons,
    SlideInfo,
)


class SlideReaderBase:
    def __init__(self, filepath):
        self.filepath = filepath

    @classmethod
    def get_supported_formats(self):
        raise NotImplementedError

    @classmethod
    def get_reader_name(self):
        raise NotImplementedError

    def get_slide_info(self):
        return SlideInfo(
            reader=self.get_reader_name(),
            image_size=self._get_image_size(),
            tile_size=self._get_tile_size(),
            pixel_size=self._get_pixel_size(),
            channels=self._get_channels(),
            bits=self._get_bits(),
            levels=self._get_levels(),
            macro_size=self._get_macro_size(),
            label_size=self._get_label_size(),
            thumbnail_size=self._get_thumbnail_size(),
        )

    def get_version(self):
        raise NotImplementedError

    def _get_image_size(self):
        return ImageSizeWithDepth(width=-1, height=-1, depth=-1)

    def _get_tile_size(self):
        return ImageSize(width=-1, height=-1)

    def _get_pixel_size(self):
        return PixelSizeInMicrons()

    def _get_channels(self):
        return -1

    def _get_bits(self):
        return -1

    def _get_levels(self):
        return []

    def _get_macro_size(self):
        return None

    def _get_label_size(self):
        return None

    def _get_thumbnail_size(self):
        return None

    def get_macro(self):
        return None

    def get_label(self):
        return None

    def get_lthumbnail(self):
        return None


class SlideReader(SlideReaderBase):
    def get_tile(self, level, tile_x, tile_y):
        raise NotImplementedError

    def get_region(self, level, start_x, start_y, size_x, size_y):
        raise NotImplementedError


class SlideReaderAsync(SlideReaderBase):
    async def get_tile(self, level, tile_x, tile_y):
        raise NotImplementedError

    async def get_region(self, level, start_x, start_y, size_x, size_y):
        raise NotImplementedError
